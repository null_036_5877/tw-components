
// @ts-ignore
import config from './config.ts'
const i18n =  config.i18n
// @ts-ignore
import theme from './theme.ts'
export const Color = theme.Color
// @ts-ignore
export const languages = i18n ? i18n.languages.map((item) => item.key) : []
export const defaultLanguage = i18n ? i18n.defaultLanguage : ''



export const debounce = (fn: any, wait: 500)=> {
  let timeout: any = null;
  return function() {
    let context = this;
    let args = arguments;
    if (timeout) clearTimeout(timeout);
    let callNow = !timeout;
    timeout = setTimeout(() => {
      timeout = null;
    }, wait);
    if (callNow) fn.apply(context, args);
  };
}



/**
 * In an array object, traverse all parent IDs based on the value of an object.
 * @param   {array}     array     The Array need to Converted.
 * @param   {string}    current   Specify the value of the object that needs to be queried.
 * @param   {string}    parentId  The alias of the parent ID of the object in the array.
 * @param   {string}    id        The alias of the unique ID of the object in the array.
 * @return  {array}    Return a key array.
 */
export const queryPathKeys = ({ array, current, parentId, id = 'id' }: { array: any; current: any; parentId: any; id?: string }) => {
  const result = [current]
  const hashMap = new Map()
  // @ts-ignore
  array.forEach((item) => hashMap.set(item[id], item))

  const getPath = ({ current }: { current: any }) => {
    const currentParentId = hashMap.get(current)[parentId]
    if (currentParentId) {
      result.push(currentParentId)
      getPath({ current: currentParentId })
    }
  }

  getPath({ current: current })
  return result
}

/**
 * In an array of objects, specify an object that traverses the objects whose parent ID matches.
 * @param   {array}     array     The Array need to Converted.
 * @param   {string}    current   Specify the object that needs to be queried.
 * @param   {string}    parentId  The alias of the parent ID of the object in the array.
 * @param   {string}    id        The alias of the unique ID of the object in the array.
 * @return  {array}    Return a key array.
 */
export function queryAncestors({ array, current, parentId, id = 'id' }: { array: any; current: any; parentId: any; id?: string }) {
  const result = [current]
  const hashMap = new Map()
  array.forEach(({ item }: { item: any }) => hashMap.set(item[id], item))

  const getPath = ({ current }: { current: any }) => {
    const currentParentId = hashMap.get(current[id])[parentId]
    if (currentParentId) {
      result.push(hashMap.get(currentParentId))
      getPath(hashMap.get(currentParentId))
    }
  }

  getPath(current)
  return result
}

/**
 * Query which layout should be used for the current path based on the configuration.
 * @param   {layouts}     layouts   Layout configuration.
 * @param   {pathname}    pathname  Path name to be queried.
 * @return  {string}   Return frist object when query success.
 */
// export function queryLayout({layouts, pathname}: { layouts: any, pathname: any }) {
//   let result = 'public'
//
//   const isMatch = ({regepx}:{regepx:any}) => {
//     return regepx instanceof RegExp
//       ? regepx.test(pathname)
//       : pathToRegexp(regepx).exec(pathname)
//   }
//
//   for (const item of layouts) {
//     let include = false
//     let exclude = false
//     if (item.include) {
//       for (const regepx of item.include) {
//         if (isMatch(regepx)) {
//           include = true
//           break
//         }
//       }
//     }
//
//     if (include && item.exclude) {
//       for (const regepx of item.exclude) {
//         if (isMatch(regepx)) {
//           exclude = true
//           break
//         }
//       }
//     }
//
//     if (include && !exclude) {
//       result = item.name
//       break
//     }
//   }
//
//   return result
// }


export function accAdd(arg1, arg2, pN) {
  if (!arg1) {
      arg1 = 0
  }
  if (!arg2) {
      arg2 = 0
  }
  arg1 = arg1.toString()
  arg2 = arg2.toString()
  var arg1Arr = arg1.split('.')
  var arg2Arr = arg2.split('.')
  var d1 = arg1Arr.length === 2 ? arg1Arr[1] : ''
  var d2 = arg2Arr.length === 2 ? arg2Arr[1] : ''
  var maxLen = Math.max(d1.length, d2.length)
  var m = Math.pow(10, maxLen)
  var result = Number(((arg1 * m + arg2 * m) / m).toFixed(maxLen))
  var d = pN
  return typeof d === 'number' ? Number(result.toFixed(d)) : result
}

/*
函数：减法函数，用来得到精确的减法结果
说明：函数返回较为精确的减法结果。
参数：arg1：第一个加数；arg2第二个加数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数
调用：Calc.Sub(arg1,arg2)
返回值：两数相减的结果
*/
export function Subtr(arg1, arg2) {
  return accAdd(arg1, -Number(arg2), arguments[2])
}

/*
函数：乘法函数，用来得到精确的乘法结果
说明：函数返回较为精确的乘法结果。
参数：arg1：第一个乘数；arg2第二个乘数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数)
调用：Calc.Mul(arg1,arg2)
返回值：两数相乘的结果
*/
export function accMul(arg1, arg2, pType) {
  if (!arg1) {
      arg1 = 0
  }
  if (!arg2) {
      arg2 = 0
  }
  var r1 = arg1.toString()
  var r2 = arg2.toString()
  var m
  var resultVal
  var d = arguments[2]
  m = (r1.split('.')[1] ? r1.split('.')[1].length : 0) + (r2.split('.')[1] ? r2.split('.')[1].length : 0)
  resultVal = (Number(r1.replace('.', '')) * Number(r2.replace('.', ''))) / Math.pow(10, m)
  if (pType === 'money') {
      resultVal = accDiv(Math.ceil(accMul(resultVal, 100)), 100)
  }
  return typeof d !== 'number' ? Number(resultVal) : Number(resultVal.toFixed(parseInt(d)))
}

/*
函数：除法函数，用来得到精确的除法结果
说明：函数返回较为精确的除法结果。
参数：arg1：除数；arg2被除数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数)
调用：Calc.Div(arg1,arg2)
返回值：arg1除于arg2的结果
*/
export function accDiv(arg1, arg2) {
  if (!arg1) {
      arg1 = 0
  }
  if (!arg2) {
      arg2 = 0
  }
  var r1 = arg1.toString()
  var r2 = arg2.toString()
  var m
  var resultVal
  var d = arguments[2]
  m = (r2.split('.')[1] ? r2.split('.')[1].length : 0) - (r1.split('.')[1] ? r1.split('.')[1].length : 0)
  resultVal = (Number(r1.replace('.', '')) / Number(r2.replace('.', ''))) * Math.pow(10, m)
  return typeof d !== 'number' ? Number(resultVal) : Number(resultVal.toFixed(parseInt(d)))
}

