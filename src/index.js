import {
    ElAffix,
    ElAlert,
    ElAutocomplete,
    ElAutoResizer,
    ElAvatar,
    ElBacktop,
    ElBadge,
    ElBreadcrumb,
    ElBreadcrumbItem,
    ElButton,
    ElButtonGroup,
    ElCalendar,
    ElCard,
    ElCarousel,
    ElCarouselItem,
    ElCascader,
    ElCascaderPanel,
    ElCheckTag,
    ElCheckbox,
    ElCheckboxButton,
    ElCheckboxGroup,
    ElCol,
    ElCollapse,
    ElCollapseItem,
    ElCollapseTransition,
    ElColorPicker,
    ElConfigProvider,
    ElContainer,
    ElAside,
    ElFooter,
    ElHeader,
    ElMain,
    ElDatePicker,
    ElDescriptions,
    ElDescriptionsItem,
    ElDialog,
    ElDivider,
    ElDrawer,
    ElDropdown,
    ElDropdownItem,
    ElDropdownMenu,
    ElEmpty,
    ElForm,
    ElFormItem,
    ElIcon,
    ElImage,
    ElImageViewer,
    ElInput,
    ElInputNumber,
    ElLink,
    ElMenu,
    ElMenuItem,
    ElMenuItemGroup,
    ElSubMenu,
    ElPageHeader,
    ElPagination,
    ElPopconfirm,
    ElPopover,
    ElPopper,
    ElProgress,
    ElRadio,
    ElRadioButton,
    ElRadioGroup,
    ElRate,
    ElResult,
    ElRow,
    ElScrollbar,
    ElSelect,
    ElOption,
    ElOptionGroup,
    ElSelectV2,
    ElSkeleton,
    ElSkeletonItem,
    ElSlider,
    ElSpace,
    ElStatistic,
    ElCountdown,
    ElSteps,
    ElStep,
    ElSwitch,
    ElTable,
    ElTableColumn,
    ElTableV2,
    ElTabs,
    ElTabPane,
    ElTag,
    ElText,
    ElTimePicker,
    ElTimeSelect,
    ElTimeline,
    ElTimelineItem,
    ElTooltip,
    ElTransfer,
    ElTree,
    ElTreeSelect,
    ElTreeV2,
    ElUpload,
    ElMessage,
    ElMessageBox,
    ElNotification
} from 'element-plus'

// 引入封装好的组件CountTo
import ContentMain from "./components/ContentMain/ContentMain.vue";
import ContentMainVirtualized from "./components/ContentMainVirtualized/ContentMainVirtualized.vue";
import CountTo from "./components/CountTo/CountTo.vue";
import TwCustomForm from "./components/TwCustomForm/TwCustomForm.vue";
import TwDialog from "./components/TwDialog/TwDialog.vue";
import TwDrawer from "./components/TwDrawer/TwDrawer.vue";
import TwForm from "./components/TwForm/TwForm.vue";
import TwFormItem from "./components/TwFormItem/TwFormItem.vue";
import TwPagination from "./components/TwPagination/TwPagination.vue";
import TwTable from "./components/TwTable/TwTable.vue";
import TwVirtualizedTable from "./components/TwVirtualizedTable/TwVirtualizedTable.vue";

const coms = {ContentMain, ContentMainVirtualized, CountTo, TwCustomForm, TwDialog, TwDrawer, TwForm, TwFormItem, TwPagination, TwTable, TwVirtualizedTable,ElAffix, ElAlert, ElAutocomplete, ElAutoResizer, ElAvatar, ElBacktop, ElBadge, ElBreadcrumb, ElBreadcrumbItem, ElButton, ElButtonGroup, ElCalendar, ElCard, ElCarousel, ElCarouselItem, ElCascader, ElCascaderPanel, ElCheckTag, ElCheckbox, ElCheckboxButton, ElCheckboxGroup, ElCol, ElCollapse, ElCollapseItem, ElCollapseTransition, ElColorPicker, ElConfigProvider, ElContainer, ElAside, ElFooter, ElHeader, ElMain, ElDatePicker, ElDescriptions, ElDescriptionsItem, ElDialog, ElDivider, ElDrawer, ElDropdown, ElDropdownItem, ElDropdownMenu, ElEmpty, ElForm, ElFormItem, ElIcon, ElImage, ElImageViewer, ElInput, ElInputNumber, ElLink, ElMenu, ElMenuItem, ElMenuItemGroup, ElSubMenu, ElPageHeader, ElPagination, ElPopconfirm, ElPopover, ElPopper, ElProgress, ElRadio, ElRadioButton, ElRadioGroup, ElRate, ElResult, ElRow, ElScrollbar, ElSelect, ElOption, ElOptionGroup, ElSelectV2, ElSkeleton, ElSkeletonItem, ElSlider, ElSpace, ElStatistic, ElCountdown, ElSteps, ElStep, ElSwitch, ElTable, ElTableColumn, ElTableV2, ElTabs, ElTabPane, ElTag, ElText, ElTimePicker, ElTimeSelect, ElTimeline, ElTimelineItem, ElTooltip, ElTransfer, ElTree, ElTreeSelect, ElTreeV2, ElUpload, ElMessage, ElMessageBox, ElNotification
}; // 将来如果有其它组件,都可以写到这个数组里

const components = [ContentMain, ContentMainVirtualized, CountTo, TwCustomForm, TwDialog, TwDrawer, TwForm, TwFormItem, TwPagination, TwTable, TwVirtualizedTable,ElAffix, ElAlert, ElAutocomplete, ElAutoResizer, ElAvatar, ElBacktop, ElBadge, ElBreadcrumb, ElBreadcrumbItem, ElButton, ElButtonGroup, ElCalendar, ElCard, ElCarousel, ElCarouselItem, ElCascader, ElCascaderPanel, ElCheckTag, ElCheckbox, ElCheckboxButton, ElCheckboxGroup, ElCol, ElCollapse, ElCollapseItem, ElCollapseTransition, ElColorPicker, ElConfigProvider, ElContainer, ElAside, ElFooter, ElHeader, ElMain, ElDatePicker, ElDescriptions, ElDescriptionsItem, ElDialog, ElDivider, ElDrawer, ElDropdown, ElDropdownItem, ElDropdownMenu, ElEmpty, ElForm, ElFormItem, ElIcon, ElImage, ElImageViewer, ElInput, ElInputNumber, ElLink, ElMenu, ElMenuItem, ElMenuItemGroup, ElSubMenu, ElPageHeader, ElPagination, ElPopconfirm, ElPopover, ElPopper, ElProgress, ElRadio, ElRadioButton, ElRadioGroup, ElRate, ElResult, ElRow, ElScrollbar, ElSelect, ElOption, ElOptionGroup, ElSelectV2, ElSkeleton, ElSkeletonItem, ElSlider, ElSpace, ElStatistic, ElCountdown, ElSteps, ElStep, ElSwitch, ElTable, ElTableColumn, ElTableV2, ElTabs, ElTabPane, ElTag, ElText, ElTimePicker, ElTimeSelect, ElTimeline, ElTimelineItem, ElTooltip, ElTransfer, ElTree, ElTreeSelect, ElTreeV2, ElUpload,
];
const install = function (App, options) {
    components.forEach((component,i) => {
        // console.log(component.name)
        // console.log(component)
        if(component.install){
            component.install(App)
        }else {
            App.component(component.name, component);
        }
    });
};


// 对于那些没有在应用中使用模块化系统的用户，他们往往将通过 <script> 标签引用你的插件，并期待插件无需调用 Vue.use() 便会自动安装 。
// 你可以在插件最后添加如下几行代码来实现自动安装：
// 判断是否是直接引入文件，如果Vue是全局对象自动安装插件
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
    install.use(window.Vue)
}


export default { install, ...coms }; // 批量的引入
