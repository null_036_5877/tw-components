// @ts-ignore
import {resolve} from 'path'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'
import ReactivityTransform from '@vue-macros/reactivity-transform/vite'
import vueSetupExtend from 'vite-plugin-vue-setup-extend'

const resolveSrc = resolve(__dirname, 'src')
const getGuid = new Date().getTime();//随机时间戳
export const devConfig = (mode: any) => {
    return {
        server: {
            port: '9527',
            hmr: {
                overlay: true    // 默认是 true，设置为false后报错不会在以vite的形式在页面显示了
            },
            proxy: {
                "/api": {
                    // target: 'http://47.116.109.205:9527/api',   // 线上湖北平台
                    target: 'http://192.168.1.116:7700',    // 测试环境
                    changeOrigin: true,
                    rewrite: (path:any) => path.replace(/^\/api/, ""),
                },
            }
        },
        define: {
            'process.env': {
                VUE_APP_BASE_API: "",
                MODE: mode
            }
        },
        base: './',
        resolve: {
            alias: {
                '@': resolveSrc,
            },
        },
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: `@use "@/styles/element/index.scss" as *;`,
                },
            },
        },
        esbuild: {
            jsxFactory: 'h',
            jsxFragment: 'Fragment',
        },
        build: {
            sourcemap: false, // 输出.map文件,默认是false
            rollupOptions: {
                output: {
                    chunkFileNames: `static/js/[name].[hash]${getGuid}.js`,
                    entryFileNames: `static/js/[name].[hash]${getGuid}.js`,
                    assetFileNames: `static/[ext]/[name].[hash]${getGuid}.[ext]`,

                },
            }
        },
        plugins: [
            vue(),
            vueSetupExtend(),
            ReactivityTransform(),
            vueJsx({
                // options are passed on to @vue/babel-plugin-jsx
            }),
            AutoImport({
                imports: ['vue', 'vue/macros', 'vue-router', '@vueuse/core', 'pinia'],
                dts: 'src/atuoFlies/auto-imports.d.ts',
                resolvers: [
                    // 自动导入图标组件
                    IconsResolver({
                        prefix: 'Icon',
                    }),
                    ElementPlusResolver()
                ],
            }),
            Components({
                dirs: ['@/element-plus/es/components', 'src/components'],
                dts: 'src/atuoFlies/components.d.ts',
                resolvers: [
                    // 自动注册图标组件
                    IconsResolver({
                        enabledCollections: ['ep'],
                    }),
                    // 自动导入 Element Plus 组件
                    ElementPlusResolver(),
                ],
            }),
            Icons({
                autoInstall: true,
            }),
            // Inspect(),  // 页面流程分析图（会直接影响启动速度）
        ],
    }
}
